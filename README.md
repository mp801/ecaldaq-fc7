# ecaldaq-fc7


mkdir [my_working_dir]

cd [my_working_dir]

git clone https://gitlab.cern.ch/fc7/fc7-firmware.git -b fc7_6.0.0

git clone https://github.com/ipbus/ipbus-firmware.git -b v1.4

git clone https://gitlab.cern.ch/mp801/ecaldaq-fc7.git

vivado ecaldaq-fc7/prj/fc7_ecaldaq/fc7_ecaldaq.xpr
