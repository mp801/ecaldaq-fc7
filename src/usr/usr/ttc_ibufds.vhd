library ieee;
use ieee.std_logic_1164.all;
--! xilinx packages
library unisim;
use unisim.vcomponents.all;

entity ttc_ibufds is
port
(
	clk		: in 	std_logic;
	i			: in 	std_logic;
	ib			: in 	std_logic;
	o			: out	std_logic
);
	
end ttc_ibufds;

architecture rtl of ttc_ibufds is

	signal ttc_rx_pre_iob			: std_logic;
	signal ttc_rx_iob					: std_logic;

	attribute keep						: boolean;
	attribute keep of ttc_rx_iob	: signal is true;



begin

iob_in: process(clk)
begin
if rising_edge(clk) then
	ttc_rx_iob <= ttc_rx_pre_iob;
end if;
end process;

	o <= ttc_rx_iob;


ttc_rxbuf: ibufds 
generic map
(
	capacitance 		=> "dont_care", 
	diff_term 			=> true, 
	dqs_bias 			=> "false", 
	ibuf_delay_value 	=> "0", 
	ibuf_low_pwr 		=> true,
	ifd_delay_value  	=> "auto",
	iostandard  		=> "lvds_25"
)
port map
(
	o 	=> ttc_rx_pre_iob, 
	i 	=> i, 
	ib => ib
);

end rtl;