library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


use work.ipbus.all;
use work.system_package.all;
--! user packages
use work.user_package.all;
use work.user_version_package.all;

use work.ttc_pg_pkg.all;
use work.ttc_codec_pkg.all;


library unisim;
use unisim.vcomponents.all;

entity user_logic is
port
(
 fmc_l12_spare0    : out std_logic;
 fmc_l12_spare2    : out std_logic;

 --# led
 usrled1_r               : out std_logic; -- fmc_l12_spare[8]
 usrled1_g               : out std_logic; -- fmc_l12_spare[9]
 usrled1_b               : out std_logic; -- fmc_l12_spare[10]
 usrled2_r               : out std_logic; -- fmc_l12_spare[11]
 usrled2_g               : out std_logic; -- fmc_l12_spare[12]
 usrled2_b               : out std_logic; -- fmc_l12_spare[13]

 --# on-board fabric clk
 fabric_clk_p            : in std_logic; -- new port [PV 2015.08.19]
 fabric_clk_n            : in std_logic; -- new port [PV 2015.08.19]
 fabric_coax_or_osc_p    : in std_logic;
 fabric_coax_or_osc_n    : in std_logic;

 --# on-board mgt clk
 pcie_clk_p              : in std_logic;
 pcie_clk_n              : in std_logic;
 osc_xpoint_a_p          : in std_logic;
 osc_xpoint_a_n          : in std_logic;
 osc_xpoint_b_p          : in std_logic;
 osc_xpoint_b_n          : in std_logic;
 osc_xpoint_c_p          : in std_logic;
 osc_xpoint_c_n          : in std_logic;
 osc_xpoint_d_p          : in std_logic;
 osc_xpoint_d_n          : in std_logic;
 ttc_mgt_xpoint_a_p      : in std_logic;
 ttc_mgt_xpoint_a_n      : in std_logic;
 ttc_mgt_xpoint_b_p      : in std_logic;
 ttc_mgt_xpoint_b_n      : in std_logic;
 ttc_mgt_xpoint_c_p      : in std_logic;
 ttc_mgt_xpoint_c_n      : in std_logic;

 --# fmc mgt clk
 fmc_l12_gbtclk0_a_p     : in std_logic;
 fmc_l12_gbtclk0_a_n     : in std_logic;
 fmc_l12_gbtclk1_a_p     : in std_logic;
 fmc_l12_gbtclk1_a_n     : in std_logic;
 fmc_l12_gbtclk0_b_p     : in std_logic;
 fmc_l12_gbtclk0_b_n     : in std_logic;
 fmc_l12_gbtclk1_b_p     : in std_logic;
 fmc_l12_gbtclk1_b_n     : in std_logic;
 fmc_l8_gbtclk0_p        : in std_logic;
 fmc_l8_gbtclk0_n        : in std_logic;
 fmc_l8_gbtclk1_p        : in std_logic;
 fmc_l8_gbtclk1_n        : in std_logic;

 --# fmc mgt
 fmc_l12_dp_c2m_p        : out std_logic_vector(11 downto 0);
 fmc_l12_dp_c2m_n        : out std_logic_vector(11 downto 0);
 fmc_l12_dp_m2c_p        : in  std_logic_vector(11 downto 0);
 fmc_l12_dp_m2c_n        : in  std_logic_vector(11 downto 0);
 fmc_l8_dp_c2m_p         : out std_logic_vector( 7 downto 0);
 fmc_l8_dp_c2m_n         : out std_logic_vector( 7 downto 0);
 fmc_l8_dp_m2c_p         : in  std_logic_vector( 7 downto 0);
 fmc_l8_dp_m2c_n         : in  std_logic_vector( 7 downto 0);
 
 --# fmc fabric clk
 fmc_l8_clk0             : in std_logic;
 fmc_l8_clk1             : in std_logic;
 fmc_l12_clk0            : in std_logic;
 fmc_l12_clk1            : in std_logic;

 --# fmc gpio
 fmc_l8_la_p             : inout std_logic_vector(33 downto 0);
 fmc_l8_la_n             : inout std_logic_vector(33 downto 0);
 fmc_l12_la_p            : inout std_logic_vector(33 downto 0);
 fmc_l12_la_n            : inout std_logic_vector(33 downto 0);

 --# amc mgt
 k7_amc_rx_p             : inout std_logic_vector(15 downto 1);
 k7_amc_rx_n             : inout std_logic_vector(15 downto 1);
 amc_tx_p                : inout std_logic_vector(15 downto 1);
 amc_tx_n                : inout std_logic_vector(15 downto 1);

 --# amc fabric
 k7_fabric_amc_rx_p03    : inout std_logic;
 k7_fabric_amc_rx_n03    : inout std_logic;
 k7_fabric_amc_tx_p03    : inout std_logic;
 k7_fabric_amc_tx_n03    : inout std_logic;

 --# ddr3
 ddr3_sys_clk_p          : in    std_logic;
 ddr3_sys_clk_n          : in    std_logic;
 ddr3_dq                 : inout std_logic_vector( 31 downto 0);
 ddr3_dqs_p              : inout std_logic_vector(  3 downto 0);
 ddr3_dqs_n              : inout std_logic_vector(  3 downto 0);
 ddr3_addr               : out   std_logic_vector( 13 downto 0);
 ddr3_ba                 : out   std_logic_vector(  2 downto 0);
 ddr3_ras_n              : out   std_logic;
 ddr3_cas_n              : out   std_logic;
 ddr3_we_n               : out   std_logic;
 ddr3_reset_n            : out   std_logic;
 ddr3_ck_p               : out   std_logic_vector(  0 downto 0);
 ddr3_ck_n               : out   std_logic_vector(  0 downto 0);
 ddr3_cke                : out   std_logic_vector(  0 downto 0);
 ddr3_cs_n               : out   std_logic_vector(  0 downto 0);
 ddr3_dm                 : out   std_logic_vector(  3 downto 0);
 ddr3_odt                : out   std_logic_vector(  0 downto 0);

 --# cdce
 cdce_pll_lock_i         : in  std_logic; -- new port [PV 2015.08.19]
 cdce_pri_clk_bufg_o     : out std_logic; -- new port [PV 2015.08.19]
 cdce_ref_sel_o          : out std_logic; -- new port [PV 2015.08.19]
 cdce_pwrdown_o          : out std_logic; -- new port [PV 2015.08.19]
 cdce_sync_o             : out std_logic; -- new port [PV 2015.08.19]
 cdce_sync_clk_o         : out std_logic; -- new port [PV 2015.08.19]

 --# system clk
 osc125_a_bufg_i         : in std_logic;
 osc125_a_mgtrefclk_i    : in std_logic;
 osc125_b_bufg_i         : in std_logic;
 osc125_b_mgtrefclk_i    : in std_logic;
 clk_31_250_bufg_i       : in std_logic; -- new port [PV 2015.08.19]

 --# ipbus comm
 ipb_clk_o               : out std_logic;
 ipb_rst_i               : in  std_logic;
 ipb_miso_o              : out ipb_rbus_array(0 to nbr_usr_slaves-1);
 ipb_mosi_i              : in  ipb_wbus_array(0 to nbr_usr_slaves-1);

 --# ipbus conf
 ip_addr_o               : out std_logic_vector(31 downto 0);
 mac_addr_o              : out std_logic_vector(47 downto 0);
 rarp_en_o               : out std_logic;
 use_i2c_eeprom_o        : out std_logic
);
end user_logic;

architecture usr of user_logic is

 signal ipb_clk                : std_logic;

 signal fabric_osc_clk_pre_buf : std_logic;
 signal fabric_osc_clk         : std_logic;

 signal fabric_clk_pre_buf     : std_logic;
 signal fabric_clk             : std_logic;

 signal ttc_pre_buf            : std_logic;
 signal ttc_clk                : std_logic;

 signal ctrl_reg               : array_32x32bit;
 signal stat_reg               : array_32x32bit;

 signal cdce_pri_clk_bufg      : std_logic;

 signal cdce_sync_from_ipb     : std_logic;
 signal cdce_sel_from_ipb      : std_logic;
 signal cdce_pwrdown_from_ipb  : std_logic;
 signal cdce_ctrl_from_ipb     : std_logic;


 signal mmcm_clk40             : std_logic;
 signal mmcm_locked            : std_logic;

 signal bc0                    : std_logic;
 signal ocr                    : std_logic;

 signal bc0_count              : integer range 0 to 3564    := 0;
 signal bc0_locked             : std_logic;
 signal bc0_clk1               : std_logic;
 signal bc0_clk2               : std_logic;
 
 signal orbit_counter          : unsigned(31 downto 0)      := to_unsigned(0   , 32);
 signal bunch_counter          : unsigned(11 downto 0)      := to_unsigned(3564, 12);


begin


 --===========================================--
 -- ipbus management
 --===========================================--
 ipb_clk           <= clk_31_250_bufg_i;  -- select the frequency of the ipbus clock
 ipb_clk_o         <= ipb_clk;    -- always forward the selected ipb_clk to system core
--
 ip_addr_o         <= x"c0_a8_00_50";
 mac_addr_o        <= x"aa_bb_cc_dd_ee_50";
 rarp_en_o         <= '1';
 use_i2c_eeprom_o  <= '1';
 --===========================================--


 --===========================================--
 -- other clocks
 --===========================================--
 -- free running 40.08MHz oscillator clock
 fclk_osc_ibuf:  ibufgds     port map (i => fabric_coax_or_osc_p, ib => fabric_coax_or_osc_n, o => fabric_osc_clk_pre_buf);
 fclk_osc_bufg:  bufg        port map (i => fabric_osc_clk_pre_buf,                           o => fabric_osc_clk);
 -- FCLKA 40.08MHz clock via x-point switch
 fclk_ibuf:      ibufgds     port map (i => fabric_clk_p, ib => fabric_clk_n, o => fabric_clk_pre_buf);
 fclk_bufg:      bufg        port map (i => fabric_clk_pre_buf,               o => fabric_clk);
 -- FCLKA-derived 160.32MHz clock from CDCE via x-point switch
 ttcclk_ibuf:    ibufds_gte2 port map (i => ttc_mgt_xpoint_b_p, ib => ttc_mgt_xpoint_b_n, o => ttc_pre_buf, ceb => '0');
 ttcclk_bufg:    bufg        port map (i => ttc_pre_buf,                                  o => ttc_clk);
 --===========================================--


 --===================================--
 cdce_synch: entity work.cdce_synchronizer
 --===================================--
 generic map
 (
     pwrdown_delay     => 1000,
     sync_delay        => 1000000
 )
 port map
 (
     reset_i           => ipb_rst_i,
     ------------------
     ipbus_ctrl_i      => not cdce_ctrl_from_ipb,          -- default: 1 (ipb controlled)
     ipbus_sel_i       =>     cdce_sel_from_ipb,           -- default: 0 (select secondary clock)
     ipbus_pwrdown_i   => not cdce_pwrdown_from_ipb,       -- default: 1 (powered up)
     ipbus_sync_i      => not cdce_sync_from_ipb,          -- default: 1 (disable sync mode), rising edge needed to resync
     ------------------
     user_sel_i        => '1', -- cdce_sel_from_fabric     -- effective only when ipbus_ctrl_i = '0'
     user_pwrdown_i    => '1', -- cdce_pwrdown_from_fabric -- effective only when ipbus_ctrl_i = '0'
     user_sync_i       => '1', -- cdce_sync_from_fabric    -- effective only when ipbus_ctrl_i = '0'
     ------------------
     pri_clk_i         => cdce_pri_clk_bufg,
     sec_clk_i         => fabric_clk,         -- copy of what is actually send to the secondary clock input
     pwrdown_o         => cdce_pwrdown_o,
     sync_o            => cdce_sync_o,
     ref_sel_o         => cdce_ref_sel_o,
     sync_clk_o        => cdce_sync_clk_o
 );

 cdce_pri_clk_bufg_o <= cdce_pri_clk_bufg; cdce_pri_clk_bufg   <= '0'; -- [note: in this example, the cdce_pri_clk_bufg is not used]
 --===================================--


 --===========================================--
 stat_regs_inst: entity work.ipb_user_status_regs
 --===========================================--
 port map
 (
     clk          => ipb_clk,
     reset        => ipb_rst_i,
     ipb_mosi_i   => ipb_mosi_i(user_ipb_stat_regs),
     ipb_miso_o   => ipb_miso_o(user_ipb_stat_regs),
     regs_i       => stat_reg
 );
 --===========================================--


 --===========================================--
 ctrl_regs_inst: entity work.ipb_user_control_regs
 --===========================================--
 port map
 (
     clk          => ipb_clk,
     reset        => ipb_rst_i,
     ipb_mosi_i   => ipb_mosi_i(user_ipb_ctrl_regs),
     ipb_miso_o   => ipb_miso_o(user_ipb_ctrl_regs),
     regs_o       => ctrl_reg
 );
 --===========================================--


 --===========================================--
 -- example register mapping
 --===========================================--
 stat_reg(0)    <= usr_id_0;
 stat_reg(1)    <= x"0000_0000";
 stat_reg(2)    <= firmware_id;




 --===========================================--
 mmcm_inst : entity work.main_mmcm
 --===========================================--
 port map
 (
     clk_out1  => mmcm_clk40,
     clk_out2  => open,
     reset     => '0',
     locked    => mmcm_locked,
     clk_in1   => fabric_clk --40mhz from fclka
 );


 -- BACKPLANE TTC in

 t0_ibuf: entity work.ttc_ibufds port map (clk => mmcm_clk40, o => bc0, i => amc_tx_p(14),          ib => amc_tx_n(14));
 t1_ibuf: entity work.ttc_ibufds port map (clk => mmcm_clk40, o => ocr, i => k7_fabric_amc_tx_p03,  ib => k7_fabric_amc_tx_n03);


 --=================================================--
 bc0_check: process (mmcm_clk40)
 --=================================================--
 
 begin

     if rising_edge(mmcm_clk40) then

         bc0_clk1 <= bc0;
         bc0_clk2 <= bc0_clk1;

         if ( bc0_clk1 = '1' and bc0_clk2 = '0' ) then

             if ( bc0_count = 3564 - 1 ) then
                 bc0_locked  <= '1';
             else
                 bc0_locked  <= '0';
             end if;

             bc0_count       <= 0;

         else

             if ( bc0_count = 3564 ) then
                 bc0_locked  <= '0';
                 bc0_count   <= 3564;
             else
                 bc0_count   <= bc0_count + 1;
             end if;

         end if;

     end if;

 end process;
 --=================================================--


 --=================================================--
 sync_counters: process(mmcm_clk40)
 --=================================================--

 begin

    if rising_edge(mmcm_clk40) then

      if bc0 = '1' then
        orbit_counter <= orbit_counter + 1;
        bunch_counter <= (others => '0');

      elsif ocr = '1' then
        orbit_counter <= (others => '0');
        bunch_counter <= to_unsigned(3564, bunch_counter'length);

      elsif bunch_counter < 3564 then
        bunch_counter <= bunch_counter + 1;        
      end if;

    end if;
	
 end process;
--=================================================--


 -- User LEDs

 usrled1_r <= '1';
 usrled1_g <= not mmcm_locked;
 usrled1_b <= '1';

 usrled2_r <= '1';
 usrled2_g <= not bc0_locked;
 usrled2_b <= '1';




 --===========================================--
 ila_inst : entity work.ila_0
 --===========================================--
 port map
 (
    clk                  => mmcm_clk40,
    probe0( 0)           => mmcm_locked,
    probe0( 1)           => bc0,
    probe0( 2)           => ocr,
    probe0( 3)           => bc0_locked,
    probe0(15 downto  4) => std_logic_vector(bunch_counter),
    probe0(47 downto 16) => std_logic_vector(orbit_counter)
 );
 --===========================================--

 
 --fmc_l12_spare0 <= mmcm_clk40;
 --fmc_l12_spare2 <= bc0;




end usr;